<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
	protected $table = 'cars';
    //
    protected $fillable = [
    	'id',
    	'brand',
    	'nos',
    	'color',
    	'type',
    	'status',
        'user_id',
    ];
}
