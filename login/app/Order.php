<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //
    protected $table = 'orders';
    //
    protected $fillable = [
    	'id',
    	'car_id',
        'user_id',
    	'brand',
    	'name',
    	'phone_number',
    	'email',
    ];

    public $timestamps = false;
}
