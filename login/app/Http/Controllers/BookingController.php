<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Car;


class BookingController extends Controller
{
    //
    public function index()
    {
        //
        $cars = Car::all();
        
        return view('cars.index', array('cars' => $cars));
        
    }

    public function show($id)
    {
        // dd($id);
        $car = Car::find($id);

        return view('cars.show', array('car' => $car));
    }

    

}
