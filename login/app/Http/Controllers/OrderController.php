<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;

class OrderController extends Controller
{
    //
    public function store(Request $request){
        // dd($request->txtId);
        $order = new Order;
        $order->car_id = $request->txtId;
        $order->brand = $request->txtBrand;
        $order->name = $request->txtName;
        $order->phone_number = $request->txtPhoneNumber;
        $order->email = $request->txtEmail;
        $order->user_id = $request->txtUserId;
        $order->save();
        return view('/homepage');
    }

    public function index()
    {
        //
        $orders = Order::all();
        return view('orders.index', array('orders' => $orders));
        
    }

    public function history()
    {
        //
        $orders = Order::all();
        return view('/history', array('orders' => $orders));
    }


}
