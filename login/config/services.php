<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
    'client_id' => '353128458392892',
    'client_secret' => '1ca16c8c57de49c184612c00220a4eba',
    'redirect' => 'http://localhost:8000/auth/facebook/callback',
    ],

    'google' => [
    'client_id' => '779732377528-d0q9u491tt88djhsao9cui55jp32g7g3.apps.googleusercontent.com',
    'client_secret' => 'x7H896BBi56O0b7Nm4EARp2S',
    'redirect' => 'http://localhost:8000/auth/google/callback',
    ],


];
