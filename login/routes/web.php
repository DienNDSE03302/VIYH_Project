<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
//Route homepage
Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', 'HomeController@index');

Route::get('/homepage', function () {
    return view('homepage');
});

Route::get('/loginpage', function () {
    return view('loginpage');
});

//Route Đăng nhập
Route::get('/registed', 'HomeController@registed');

Route::get('auth/{provider}', 'Auth\RegisterController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\RegisterController@handleProviderCallback');

Auth::routes();


//Route booking
Route::resource('cars', 'BookingController');

Route::resource('orders', 'OrderController');

Route::get('/history','OrderController@history');

