@extends('layouts.app')

@section('content')
<head>
    <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 30px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
</head>
<body>
    <center>
    <div class="title m-b-md">
            Quản lý danh sách 
    </div>    
        <table class="table table-bordered" border="2">
        <thead>
                <th>Id của xe</th>
                <th>Tên xe</th>
                <th>Tên người đặt</th>
                <th>Số điện thoại</th>
                <th>Email</th>
        </thead>
        @foreach ($orders as $order)
            @if((Auth::user()->id) == ($order->user_id))         
                    <tbody>
                        <tr>
                            <td>{{ $order->car_id }}</td>
                            <td>{{ $order->brand }}</td>
                            <td>{{ $order->name }}</td>
                            <td>{{ $order->phone_number }}</td>
                            <td>{{ $order->email }}</td>
                            <td><a href="">Xóa</a></td>
                        </tr>
                    </tbody>
            @endif
        @endforeach
        </table>
<body>
@endsection
