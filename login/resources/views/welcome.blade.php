<!DOCTYPE html>
<html lang="en"><head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="css/normalize.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <style>
  .carousel-inner > .item > img,
  .carousel-inner > .item > a > img {
      width: 70%;
      margin: auto;
  }
  </style>
</head>
<body>
<!--head-->
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Vehicle In Your Hand</a>
    </div>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="/register"><span class="glyphicon glyphicon-user"></span> Đăng kí</a></li>
      <li><a href="/loginpage"><span class="glyphicon glyphicon-log-in"></span> Đăng Nhập</a></li>
    </ul>
    <form class="navbar-form navbar-left">
      <div class="form-group">
        <input type="text" class="form-control" placeholder="Search">
      </div>
      <button type="submit" class="btn btn-default">Submit</button>
    </form>
  </div>
</nav>
<!--anh dong-->
<div class="container-fluid">
  
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class=""></li>
      <li data-target="#myCarousel" data-slide-to="1" class=""></li>
      <li data-target="#myCarousel" data-slide-to="2" class=""></li>
      <li data-target="#myCarousel" data-slide-to="3" class="active"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">

      <div class="item">
        <img src="fonts/xe 1.jpg" alt="Chania" width="460" height="345">
        <div class="carousel-caption">
          <h3>Chevrolet</h3>
        </div>
      </div>

      <div class="item">
        <img src="fonts/xe 2.jpg" alt="Chania" width="1920" height="345">
        <div class="carousel-caption">
          <h3>Mansory</h3>
        </div>
      </div>
    
      <div class="item">
        <img src="fonts/xe 3.jpg" alt="Flower" width="1920" height="345">
        <div class="carousel-caption">
          <h3>Lamborghini</h3>
        </div>
      </div>

      <div class="item active">
        <img src="fonts/xe 4.jpg" alt="Flower" width="1920" height="345">
        <div class="carousel-caption">
          <h3>Rolls-royce</h3>
        </div>
      </div>
  
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
  <div class="row">
  <div class="col-sm-2"></div>
  <div class="col-sm-8" id="content"></div>
	<div class="container">
  <a href="#"><h2>Những xe được tìm kiếm nhiều nhất</h2></a>
  
  <div class="row">
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="fonts/audi.jpg" target="_blank">
          <img src="fonts/audi.jpg" alt="Lights" style="width:100%">
          <div class="caption">
            <p>Audi Q8</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="fonts/d5.jpg" target="_blank">
          <img src="fonts/d5.jpg" alt="Nature" style="width:200%">
          <div class="caption">
            <p>Roll Royce</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="fonts/d6.jpg" target="_blank">
          <img src="fonts/d6.jpg" alt="Fjords" style="width:100%">
          <div class="caption">
            <p>Chevrolet</p>
          </div>
        </a>
      </div>
    </div>
  </div>
</div>
<div class="container">
  <a href="#"><h2>Những xe giá rẻ nhất </h2></a>
  <div class="row">
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="fonts/d8.jpg" target="_blank">
          <img src="fonts/d8.jpg" alt="Lights" style="width:100%">
          <div class="caption">
            <p>300.000đ/ngày</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="fonts/d10.jpg" target="_blank">
          <img src="fonts/d10.jpg" alt="Nature" style="width:200%">
          <div class="caption">
            <p>450.000đ/ngày</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="fonts/d13.jpg" target="_blank">
          <img src="fonts/d13.jpg" alt="Fjords" style="width:100%">
          <div class="caption">
            <p>600.000đ/ngày</p>
          </div>
        </a>
      </div>
    </div>
  </div>
</div>
	<div class="container">
  <a href="#"><h2>Những xe gần bạn nhất </h2></a>
  <div class="row">
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="fonts/d12.jpg" target="_blank">
          <img src="fonts/d12.jpg" alt="Lights" style="width:100%">
          <div class="caption">
            <p>1.5km</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="fonts/d4.jpg" target="_blank">
          <img src="fonts/d4.jpg" alt="Nature" style="width:200%">
          <div class="caption">
            <p>4km</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="fonts/d7.jpg" target="_blank">
          <img src="fonts/d7.jpg" alt="Fjords" style="width:100%">
          <div class="caption">
            <p>11km</p>
          </div>
        </a>
      </div>
    </div>
  </div>
</div>

</body>
</html>
