@extends('layouts.app')

@section('content')
<head>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 40px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            .form {
                border: 5;
                background-color: #f9f9f9;
                display: block;
                width: 200px;
                height: 120px;
                text-align: center;
                margin: 20px auto 0 auto;
                display: none;
            }            
        </style>
</head>
<body>
        <center>
            <div class="title m-b-md">
                    Thông số chi tiết về xe {{ $car->brand }}
            </div>    
                 <table>
                     <tr>
                        <td>Hãng xe: </td>
                        <td>{{ $car->brand }}</td>
                     </tr>
                     <tr>
                        <td>Số chỗ: </td>
                        <td>{{ $car->nos }}</td>                          
                     </tr>
                     <tr>
                        <td>Màu sắc: </td>
                        <td>{{ $car->color }}</td> 
                     </tr>
                     <tr>
                        <td>Loại xe(tự lái hay có người lái): </td>
                        <td>{{ $car->type }}</td>
                     </tr>
                     <tr>
                        <td>Trạng thái: </td>
                        <td>{{ $car->status }}</td>
                     </tr>
                 </table>
                 <button type="button" class="btn btn-default" id="btnBooking1">Đặt xe ngay</button>
                 <button class="btn btn-default" onclick="window.location.href='/cars'">Quay trở lại</button>
        </center>
        <center>
                <div class="form" id="detailForm">
                    <form method="POST" action="{!! route('orders.store') !!}" name="frmAdd">
                        <div class="form-group">
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                        </div>
                        <div class="form-group">
                        <input type="text" name="txtName" class="form-control" placeholder="Họ và tên..." style="width: 200px;" value="{{ Auth::user()->name }}" />
                        </div>
                        <div class="form-group">
                        <input type="text" name="txtPhoneNumber" class="form-control" placeholder="Số điện thoại..." style="width: 200px;"/>
                        </div>
                        <div class="form-group">
                        <input type="text" name="txtEmail" class="form-control" placeholder="Email..." style="width: 200px;" value="{{ Auth::user()->email }}"/>
                        </div>
                        </br>
                        <input type="hidden" name="txtId" class="form-control" value="{{ $car->id }}"/>
                        <input type="hidden" name="txtBrand" class="form-control" value="{{ $car->brand }}"/>
                        <input type="hidden" name="txtUserId" class="form-control" value="{{ $car->user_id }}"/>
                        <button type="submit" class="btn btn-default">Đặt xe</button>
                    </form>
                </div>
        </center>
        <script>
             $("#btnBooking1").click(function(){
                $("#detailForm").show();
            });
        </script>
<body>
@endsection
