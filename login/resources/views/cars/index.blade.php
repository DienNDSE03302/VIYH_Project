@extends('layouts.app')

@section('content')
<head>
    <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 30px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
</head>
<body>
    <center>
    <div class="title m-b-md">
            Bấm để xem thông tin chi tiết về xe
    </div>    
        <table class="table table-bordered" border="2">
        <thead>
                <th>Mã</th>
                <th>Tên</th>
                <th>Trạng thái</th>
                <th>Màu sắc</th>
                <th></th>
        </thead>

        @foreach ($cars as $car)
            
            <tbody>
                <tr>
                    <td>{{ $car->id }}</td>
                    <td>{{ $car->brand }}</td>
                    <td>{{ $car->status }}</td>
                    <td>{{ $car->color }}</td>
                    <td><a href="http://localhost:8000/cars/{{ $car->id }}">Chi tiết</a></td>
                </tr>
            </tbody>
        @endforeach
        </table>
<body>
@endsection
