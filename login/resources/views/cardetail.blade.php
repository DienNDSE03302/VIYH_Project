<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Vehicle in your hand</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

        <!-- Styles -->
        <!-- <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style> -->
        <style>
            html {
                box-sizing: border-box;
            }
            *,
            *::before,
            *::after {
                box-sizing: inherit;
            }
            .button {
                padding: 10px 5px;
                background-color: green;
                color: #fff;
            }
            .form {
                border: 1px solid #ccc;
                background-color: #f9f9f9;
                display: block;
                width: 400px;
                height: 150px;
                text-align: center;
                margin: 20px auto 0 auto;
                display: none;
            }
        </style>
        <center><h2 class="title m-b-md">
            Car information detail
        </h2></center>
    </head>
    <body>
         <center>
         <table border="2">
             <tr>
                <td>Hãng xe: </td>
                <td>   </td>
             </tr>
             <tr>
                <td>Số chỗ: </td>
                <td>   </td>                          
             </tr>
             <tr>
                <td>Màu sắc: </td>
                <td>   </td> 
             </tr>
             <tr>
                <td>Loại xe(tự lái hay có người lái): </td>
                <td>   </td>
             </tr>
         </table>
         <button class="button" type="button" id="btnBooking">Đặt xe</button>
         <button class="button" type="button">Quay trở lại</button>
         </center>
        <div class="form" id="detailForm">
            <form action="" style="height: 150px; width: 300px;">
                <center>
                <tr>
                    <td>Họ và Tên</td>
                    <td><input type="text" name="txtName"/></td>
                </tr>
                <tr>
                    <td>Số điện thoại</td>
                    <td><input type="text" name="txtPhoneNumber"/></td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td><input type="text" name="txtEmail"/></td>
                </tr>
                <button type="button" class="button" style="margin: 0 auto;">Book</button>
                </center>
            </form>
        </div>
        <script>
             $("#btnBooking").click(function(){
                $("#detailForm").show();
            });
        </script>

    </body>
</html>
